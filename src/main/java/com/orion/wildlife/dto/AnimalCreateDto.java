package com.orion.wildlife.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnimalCreateDto {
	@NotBlank
	@Size(min = 2, max = 15)
	private String name;
	
	private UUID typeId;
	
	private UUID areaId;
}
