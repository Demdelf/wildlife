package com.orion.wildlife.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnimalDto {
	private UUID id;
	private String name;
	private AnimalTypeDtoShort type;
	private TerritoryAreaDtoShort area;
}
