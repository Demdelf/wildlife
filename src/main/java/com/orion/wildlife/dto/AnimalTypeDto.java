package com.orion.wildlife.dto;

import com.orion.wildlife.domain.AnimalClass;
import com.orion.wildlife.domain.GroupOfPopulation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnimalTypeDto {
	private UUID id;
	private String name;
	private AnimalClass animalClass;
	private GroupOfPopulation group;
}
