package com.orion.wildlife.dto;

import com.orion.wildlife.domain.GroupOfPopulation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TerritoryAreaDto {
	private UUID id;
	private String name;
	private String code;
	private Long square;
	private Map<GroupOfPopulation, Integer> numbersOfAnimalsOfPopulation;
}
