package com.orion.wildlife.repository;

import com.orion.wildlife.domain.AnimalType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface AnimalTypeRepository extends JpaSpecificationExecutor<AnimalType>, JpaRepository<AnimalType, UUID> {
}
