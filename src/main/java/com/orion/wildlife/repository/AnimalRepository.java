package com.orion.wildlife.repository;

import com.orion.wildlife.domain.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface AnimalRepository extends JpaSpecificationExecutor<Animal>, JpaRepository<Animal, UUID> {
}
