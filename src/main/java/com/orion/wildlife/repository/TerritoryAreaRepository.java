package com.orion.wildlife.repository;

import com.orion.wildlife.domain.TerritoryArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface TerritoryAreaRepository
		extends JpaSpecificationExecutor<TerritoryArea>, JpaRepository<TerritoryArea, UUID> {
}
