package com.orion.wildlife.util;

import com.orion.wildlife.domain.Animal;
import com.orion.wildlife.domain.AnimalType;
import com.orion.wildlife.domain.GroupOfPopulation;
import com.orion.wildlife.domain.TerritoryArea;
import com.orion.wildlife.dto.*;
import com.orion.wildlife.service.AnimalTypeService;
import com.orion.wildlife.service.TerritoryAreaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class Converter {
	
	private final AnimalTypeService animalTypeService;
	private final TerritoryAreaService territoryAreaService;
	
	/**
	 * Convert animal type to dto
	 *
	 * @param animalType animal type
	 * @return dto of animal type
	 */
	public AnimalTypeDto convertToAnimalTypeDto(AnimalType animalType){
		AnimalTypeDto dto = new AnimalTypeDto();
		dto.setId(animalType.getId());
		dto.setName(animalType.getName());
		dto.setAnimalClass(animalType.getAnimalClass());
		dto.setGroup(animalType.getGroup());
		return dto;
	}
	
	/**
	 * Convert animal creation dto to animal
	 *
	 * @param dto dto of animal creation
	 * @return animal
	 */
	public Animal convertAnimalCreateDtoToAnimal(AnimalCreateDto dto) {
		Animal animal = new Animal();
		animal.setName(dto.getName());
		animal.setType(animalTypeService.findOne(dto.getTypeId()));
		animal.setArea(territoryAreaService.findOne(dto.getAreaId()));
		
		return animal;
	}
	
	/**
	 * Convert territory area to dto
	 *
	 * @param territoryArea territory area
	 * @return dto of territory area
	 */
	public TerritoryAreaDto convertToTerritoryAreaDto(TerritoryArea territoryArea) {
		TerritoryAreaDto dto = new TerritoryAreaDto();
		dto.setId(territoryArea.getId());
		dto.setName(territoryArea.getName());
		dto.setCode(territoryArea.getCode());
		dto.setSquare(territoryArea.getSquare());
		Map<GroupOfPopulation, Integer> map = new HashMap<>();
		for (GroupOfPopulation g: GroupOfPopulation.values()
			 ) {
			map.put(g, 0);
		}
		for (Animal a: territoryArea.getAnimals()
			 ) {
			GroupOfPopulation group = a.getType().getGroup();
			map.put(group, map.get(group) + 1);
		}
		dto.setNumbersOfAnimalsOfPopulation(map);
		return dto;
	}
	
	/**
	 * Convert animal to dto
	 *
	 * @param animal animal
	 * @return dto of animal
	 */
	public AnimalDto convertToAnimalDto(Animal animal) {
		AnimalDto dto = new AnimalDto();
		
		dto.setId(animal.getId());
		dto.setName(animal.getName());
		dto.setArea(convertToTerritoryAreaDtoShort(animal.getArea()));
		dto.setType(convertToAnimalTypeDtoShort(animal.getType()));
		
		return dto;
	}
	
	/**
	 * Convert animal type to short dto
	 *
	 * @param type animal type
	 * @return short dto of animal type
	 */
	private AnimalTypeDtoShort convertToAnimalTypeDtoShort(AnimalType type) {
		AnimalTypeDtoShort dto = new AnimalTypeDtoShort();
		dto.setId(type.getId());
		dto.setName(type.getName());
		
		return dto;
	}
	
	/**
	 * Convert territory area to short dto
	 *
	 * @param area territory area
	 * @return short dto of territory area
	 */
	private TerritoryAreaDtoShort convertToTerritoryAreaDtoShort(TerritoryArea area) {
		TerritoryAreaDtoShort dto = new TerritoryAreaDtoShort();
		dto.setId(area.getId());
		dto.setName(area.getName());
		dto.setCode(area.getCode());
		dto.setSquare(area.getSquare());
		
		return dto;
	}
}
