package com.orion.wildlife.domain;

public enum AnimalClass {
	AGNATHA("jaw-less fish"),
	CHRONDRICHTYES("cartilaginous fish"),
	OSTEICHTHYES("bony fish"),
	AMPHIBIA("amphibians"),
	REPTILIA("reptiles"),
	AVES("birds"),
	MAMMALIA("mammals");
	
	String nameOnEnglish;
	
	private AnimalClass(String nameOnEnglish) {
	}
}
