package com.orion.wildlife.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "AREAS")
public class TerritoryArea extends Identifiable {
	
	/**
	 * Name
	 */
	@Column(name = "NAME")
	@NotNull
	private String name;
	
	/**
	 * Code
	 */
	@Column(name = "CODE")
	@NotNull
	private String code;
	
	/**
	 * square, in square kilometers
	 */
	@Column(name = "SQUARE")
	@NotNull
	private Long square;
	
	/**
	 * Name of the responsible person
	 */
	@Column(name = "PERSON_NAME")
	@NotNull
	private String personName;
	
	/**
	 * Phone number of the responsible person
	 */
	@Column(name = "PERSON_PHONE")
	@NotNull
	private String phoneNumber;
	
	/**
	 * Animals on this area
	 */
	@OneToMany(mappedBy = "area")
	private Set<Animal> animals;
}
