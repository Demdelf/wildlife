package com.orion.wildlife.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ANIMALS")
public class Animal extends Identifiable{
	/**
	 * Name
	 */
	@Column(name = "NAME")
	private String name;
	
	/**
	 * Animal type
	 */
	@ManyToOne
	@JoinColumn(name="TYPE_ID", nullable = false)
	private AnimalType type;
	
	/**
	 * Territory area it was observed last time
	 */
	@ManyToOne
	@JoinColumn(name="AREA_ID", nullable = false)
	private TerritoryArea area;
	
}
