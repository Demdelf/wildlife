package com.orion.wildlife.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ANIMALTYPES")
public class AnimalType extends Identifiable{
	
	/**
	 * Name
	 */
	@Column(name = "NAME")
	@NotNull
	private String name;
	
	/**
	 * Class
	 */
	@Column(name = "CLASS")
	@NotNull
	private AnimalClass animalClass;
	
	/**
	 * Group of population
	 */
	@Column(name = "GROUP_OF_POPULATION")
	@NotNull
	private GroupOfPopulation group;
	
	/**
	 * Animals of this type
	 */
	@OneToMany(mappedBy = "type")
	private Set<Animal> animals;
}
