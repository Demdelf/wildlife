package com.orion.wildlife.controller;

import com.orion.wildlife.domain.AnimalType;
import com.orion.wildlife.dto.AnimalTypeDto;
import com.orion.wildlife.service.AnimalTypeService;
import com.orion.wildlife.util.Converter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Controller for animal types
 */
@RestController
@RequestMapping("animal-type")
@RequiredArgsConstructor
public class AnimalTypeController {
	
	private final AnimalTypeService service;
	private final Converter converter;
	
	/**
	 * Get all animal types
	 *
	 * @param pageable Pagination params
	 * @return page of animal types
	 */
	@GetMapping
	public Page<AnimalTypeDto> getAllTypes(@PageableDefault(size = 3) Pageable pageable) {
		return service.findAll(pageable).map(converter::convertToAnimalTypeDto);
	}
	
	/**
	 * Create new animal type
	 *
	 * @param animalType new animal type
	 * @return response with animal type id
	 */
	@PostMapping
	public ResponseEntity<UUID> create(@RequestBody @Valid AnimalType animalType) {
		UUID id = service.create(animalType);
		return new ResponseEntity<>(id, HttpStatus.CREATED);
	}
	
	/**
	 * Get animal type
	 *
	 * @param id animal type id
	 * @return animal type
	 */
	@GetMapping("{id}")
	public AnimalTypeDto get(@PathVariable UUID id) {
		return converter.convertToAnimalTypeDto(service.findOne(id));
	}
	
	/**
	 * Update animal type
	 *
	 * @param id         animal type id
	 * @param animalType animal type data for update
	 * @return updated animal type
	 */
	@PutMapping("{id}")
	public AnimalTypeDto update(@PathVariable UUID id, @RequestBody @Valid AnimalType animalType) {
		return converter.convertToAnimalTypeDto(service.update(id, animalType));
	}
	
	/**
	 * Delete animal type
	 *
	 * @param id animal type id
	 * @return response with status
	 */
	@DeleteMapping("{id}")
	public ResponseEntity<String> delete(@PathVariable UUID id) {
		service.delete(id);
		return new ResponseEntity<>("Animal type was deleted", HttpStatus.OK);
	}
}
