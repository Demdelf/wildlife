package com.orion.wildlife.controller;

import com.orion.wildlife.domain.Animal;
import com.orion.wildlife.dto.AnimalDto;
import com.orion.wildlife.dto.AnimalCreateDto;
import com.orion.wildlife.service.AnimalService;
import com.orion.wildlife.util.Converter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Controller for animals
 */
@RestController
@RequestMapping("animals")
@RequiredArgsConstructor
public class AnimalController {
	private final AnimalService service;
	private final Converter converter;
	
	/**
	 * Get all animals
	 *
	 * @param pageable Pagination params
	 * @return page of animals
	 */
	@GetMapping
	public Page<AnimalDto> getAll(@PageableDefault(size = 3) Pageable pageable) {
		return service.findAll(pageable).map(converter::convertToAnimalDto);
	}
	
	/**
	 * Create new animal
	 *
	 * @param dto data of new animal
	 * @return response with animal id
	 */
	@PostMapping
	public ResponseEntity<UUID> create(@RequestBody @Valid AnimalCreateDto dto) {
		Animal animal = converter.convertAnimalCreateDtoToAnimal(dto);
		
		UUID id = service.create(animal);
		return new ResponseEntity<>(id, HttpStatus.CREATED);
	}
	
	/**
	 * Get animal
	 *
	 * @param id animal id
	 * @return animal
	 */
	@GetMapping("{id}")
	public AnimalDto get(@PathVariable UUID id) {
		return converter.convertToAnimalDto(service.findOne(id));
	}
	
	/**
	 * Update animal
	 *
	 * @param id  animal id
	 * @param dto animal data for update
	 * @return updated animal
	 */
	@PutMapping("{id}")
	public AnimalDto update(@PathVariable UUID id, @RequestBody @Valid AnimalCreateDto dto) {
		Animal animal = converter.convertAnimalCreateDtoToAnimal(dto);
		return converter.convertToAnimalDto(service.update(id, animal));
	}
	
	/**
	 * Delete animal
	 *
	 * @param id animal id
	 * @return response with status
	 */
	@DeleteMapping("{id}")
	public ResponseEntity<String> delete(@PathVariable UUID id) {
		service.delete(id);
		return new ResponseEntity<>("Animal was deleted", HttpStatus.OK);
	}
}
