package com.orion.wildlife.controller;

import com.orion.wildlife.domain.TerritoryArea;
import com.orion.wildlife.dto.TerritoryAreaDto;
import com.orion.wildlife.service.TerritoryAreaService;
import com.orion.wildlife.util.Converter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Controller for territory areas
 */
@RestController
@RequestMapping("territory-area")
@RequiredArgsConstructor
public class TerritoryAreaController {
	
	private final TerritoryAreaService service;
	private final Converter converter;
	
	/**
	 * Get all territory areas
	 *
	 * @param pageable Pagination params
	 * @return page of territory areas
	 */
	@GetMapping
	public Page<TerritoryAreaDto> getAllUsers(@PageableDefault(size = 3) Pageable pageable) {
		return service.findAll(pageable).map(converter::convertToTerritoryAreaDto);
	}
	
	/**
	 * Create new territory area
	 *
	 * @param territoryArea new territory area
	 * @return response with territory area id
	 */
	@PostMapping
	public ResponseEntity<UUID> create(@RequestBody @Valid TerritoryArea territoryArea) {
		UUID id = service.create(territoryArea);
		return new ResponseEntity<>(id, HttpStatus.CREATED);
	}
	
	/**
	 * Get territory area
	 *
	 * @param id territory area id
	 * @return territory area
	 */
	@GetMapping("{id}")
	public TerritoryAreaDto get(@PathVariable UUID id) {
		return converter.convertToTerritoryAreaDto(service.findOne(id));
	}
	
	/**
	 * Update territory area
	 *
	 * @param id            territory area id
	 * @param territoryArea territory area data for update
	 * @return updated territory area
	 */
	@PutMapping("{id}")
	public TerritoryAreaDto update(@PathVariable UUID id, @RequestBody @Valid TerritoryArea territoryArea) {
		return converter.convertToTerritoryAreaDto(service.update(id, territoryArea));
	}
	
	/**
	 * Delete territory area
	 *
	 * @param id territory area id
	 */
	@DeleteMapping("{id}")
	public ResponseEntity<String> delete(@PathVariable UUID id) {
		service.delete(id);
		return new ResponseEntity<>("Territory area was deleted", HttpStatus.OK);
	}
}
