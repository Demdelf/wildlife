package com.orion.wildlife.service;

import com.orion.wildlife.domain.AnimalType;
import com.orion.wildlife.repository.AnimalTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AnimalTypeService {
	private final AnimalTypeRepository repository;
	
	/**
	 * Get all animal types
	 *
	 * @param pageable Pagination params
	 * @return page of animal types
	 */
	public Page<AnimalType> findAll(Pageable pageable) {
		return repository
				.findAll(pageable);
	}
	
	/**
	 * Create and save new animal type
	 *
	 * @param animalType new animal type
	 * @return animal type id
	 */
	@Transactional
	public UUID create(AnimalType animalType) {
		animalType = repository.save(animalType);
		
		return animalType.getId();
	}
	
	/**
	 * Get animal type
	 *
	 * @param id animal type id
	 * @return animal type
	 */
	public AnimalType findOne(UUID id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find AnimalType with id: " + id));
	}
	
	/**
	 * Update animal type
	 *
	 * @param id                animal type id
	 * @param updatedAnimalType animal type data for update
	 * @return updated animal type
	 */
	@Transactional
	public AnimalType update(UUID id, AnimalType updatedAnimalType) {
		AnimalType animalType = findOne(id);
		
		animalType.setName(updatedAnimalType.getName());
		animalType.setAnimalClass(updatedAnimalType.getAnimalClass());
		animalType.setGroup(updatedAnimalType.getGroup());
		
		return repository.save(animalType);
	}
	
	/**
	 * Delete animal type
	 *
	 * @param id animal type id
	 */
	@Transactional
	public void delete(UUID id) {
		AnimalType animalType = findOne(id);
		repository.delete(animalType);
	}
}
