package com.orion.wildlife.service;

import com.orion.wildlife.domain.Animal;
import com.orion.wildlife.repository.AnimalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AnimalService {
	private final AnimalRepository repository;
	
	/**
	 * Get all animals
	 *
	 * @param pageable Pagination params
	 * @return page of animals
	 */
	public Page<Animal> findAll(Pageable pageable) {
		return repository
				.findAll(pageable);
	}
	
	/**
	 * Create and save new animal
	 *
	 * @param animal new animal
	 * @return animal id
	 */
	@Transactional
	public UUID create(Animal animal) {
		animal = repository.save(animal);
		
		return animal.getId();
	}
	
	/**
	 * Get animal
	 *
	 * @param id animal id
	 * @return animal
	 */
	public Animal findOne(UUID id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find animal with id: " + id));
	}
	
	/**
	 * Update animal
	 *
	 * @param id            animal id
	 * @param updatedAnimal animal data for update
	 * @return updated animal
	 */
	@Transactional
	public Animal update(UUID id, Animal updatedAnimal) {
		Animal animal = findOne(id);
		
		animal.setName(updatedAnimal.getName());
		animal.setArea(updatedAnimal.getArea());
		animal.setType(updatedAnimal.getType());
		
		return repository.save(animal);
	}
	
	/**
	 * Delete animal
	 *
	 * @param id animal id
	 */
	@Transactional
	public void delete(UUID id) {
		Animal animal = findOne(id);
		repository.delete(animal);
	}
}
