package com.orion.wildlife.service;

import com.orion.wildlife.domain.TerritoryArea;
import com.orion.wildlife.repository.TerritoryAreaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TerritoryAreaService {
	private final TerritoryAreaRepository repository;
	
	/**
	 * Get all territory areas
	 *
	 * @param pageable Pagination params
	 * @return page of territory areas
	 */
	public Page<TerritoryArea> findAll(Pageable pageable) {
		return repository
				.findAll(pageable);
	}
	
	/**
	 * Create and save new territory area
	 *
	 * @param territoryArea new territory area
	 * @return territory area id
	 */
	@Transactional
	public UUID create(TerritoryArea territoryArea) {
		territoryArea = repository.save(territoryArea);
		
		return territoryArea.getId();
	}
	
	
	/**
	 * Get territory area
	 *
	 * @param id territory area id
	 * @return territory area
	 */
	public TerritoryArea findOne(UUID id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find TerritoryArea with id: " + id));
	}
	
	/**
	 * Update territory area
	 *
	 * @param id          territory area id
	 * @param updatedArea territory area data for update
	 * @return updated territory area
	 */
	@Transactional
	public TerritoryArea update(UUID id, TerritoryArea updatedArea) {
		TerritoryArea area = findOne(id);
		
		area.setName(updatedArea.getName());
		area.setCode(updatedArea.getCode());
		area.setSquare(updatedArea.getSquare());
		area.setPersonName(updatedArea.getPersonName());
		area.setPhoneNumber(updatedArea.getPhoneNumber());
		
		return repository.save(area);
	}
	
	/**
	 * Delete territory area
	 *
	 * @param id territory area id
	 */
	@Transactional
	public void delete(UUID id) {
		TerritoryArea area = findOne(id);
		repository.delete(area);
	}
}
